import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ScheduleModel, EngineerModel, AllocationModel, SwapRequestModel } from '../models/schedule-model';
import { Observable } from '../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})

export class ScheduleService {
  httpOptions = {
      headers: new HttpHeaders({
          'api-version': environment.api_version,
          'accept': 'application/json'
      })
  };

  constructor(private http: HttpClient) { }

  getSchedule(month: number, year: number): Observable<ScheduleModel> {
    const url = environment.api_base + '/schedule/month/' + year + '/' + month;
    const response = this.http.get<ScheduleModel>(url, this.httpOptions);
    return response;
  }

  getEngineers(): Observable<EngineerModel[]> {
    const url = environment.api_base + '/schedule/engineer/list';
    const response = this.http.get<EngineerModel[]>(url, this.httpOptions);
    return response;
  }

  getSwapAllocations(engineerFrom: string, dateFrom: Date): Observable<AllocationModel[]> {
    const url = environment.api_base + '/schedule/swap/' + engineerFrom + '/' + dateFrom.toISOString() + '/list';
    const response = this.http.get<AllocationModel[]>(url, this.httpOptions);
    return response;
  }

  createSwap(swapRequestModel: SwapRequestModel): Observable<any> {
    const url = environment.api_base + '/schedule/swap';
    const response = this.http.post(url, swapRequestModel, this.httpOptions);
    return response;
  }

  createSchedule(month: number, year: number): Observable<any> {
    const url = environment.api_base + '/schedule/month';
    const PopulateScheduleRequestModel = {'year': year, 'month': month };
    const response = this.http.post(url, PopulateScheduleRequestModel , this.httpOptions);
    return response;
  }
}
