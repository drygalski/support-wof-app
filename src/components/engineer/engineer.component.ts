import { Component, OnInit } from '@angular/core';
import { EngineerModel } from '../../models/schedule-model';
import { ScheduleService } from 'src/services/schedule.service';
import { DataService } from 'src/services/data.service';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';


@Component({
  selector: 'app-engineer',
  templateUrl: './engineer.component.html',
  styleUrls: ['./engineer.component.css'],
  providers: [{ provide: BsDropdownConfig, useValue: { autoClose: true } }]
})
export class EngineerComponent implements OnInit {
  engineers: EngineerModel[] = [];
  selectedEngineer = null;

  selectEngineer(engineer: EngineerModel) {
    this.dataService.viewAs.next(engineer);
    this.selectedEngineer = engineer;
  }

  constructor(private scheduleService: ScheduleService, private dataService: DataService) { }

  ngOnInit() {
    this.scheduleService.getEngineers().subscribe(
      result => {this.engineers = result; }
    );
  }
}
