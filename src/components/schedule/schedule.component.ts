import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { CalendarEvent} from 'angular-calendar';
import { ScheduleService } from 'src/services/schedule.service';
import { DataService } from 'src/services/data.service';
import { environment } from 'src/environments/environment';
import { SupportDayModel, ScheduleModel, EngineerModel, AllocationModel } from 'src/models/schedule-model';
import { MonthViewDay } from '../../../node_modules/calendar-utils';
import { SwapComponent } from '../swap/swap.component';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})

export class ScheduleComponent implements OnInit {
  month: number;
  year: number;
  events: CalendarEvent[] = [];
  scheduleKeys: string[] = [];
  error: any;
  weeklyViewDate: Date;
  viewDate: Date;
  view = 'month';
  viewAs: EngineerModel;
  viewCalDate: MonthViewDay<any> = null;

  constructor(private scheduleService: ScheduleService,  private dataService: DataService) { }

  ngOnInit() {
    this.viewDate = new Date();
    this.viewDateChange();
    this.populateEvents(this.month, this.year);
    this.dataService.viewAs.subscribe((viewAs) => { this.viewAs = viewAs; });
    this.dataService.updateMonthScheduleKeys.subscribe((keys) => { this.updateScheduleMonths(keys); });
  }

  updateScheduleMonths(keys: string[]): any {
    this.events = this.events.filter((e) => e.meta.key in keys);
    this.scheduleKeys = this.scheduleKeys.filter((k) => k in keys);
    keys.forEach(key => {
      const keyValues = key.split('.');
      this.populateEvents(Number(keyValues[1]), Number(keyValues[0]));
    });
  }

  getFilteredEvents() {
    if (this.viewAs) {
      const filteredEvents = this.events.filter((e) => e.meta.id === this.viewAs.id);
      return filteredEvents;
    } else {
      return this.events;
    }
  }

  isMenuOpen() {
    return this.viewCalDate != null &&  this.viewCalDate.events != null && this.viewCalDate.events.length > 0;
  }

  viewDateChangedHandler() {
    this.viewDateChange();
    this.populateEvents(this.month, this.year);
  }

  viewDateChange() {
    this.weeklyViewDate = this.viewDate;
    this.month = this.viewDate.getMonth() + 1;
    this.year = this.viewDate.getFullYear();
  }

  setViewDate(day: MonthViewDay) {
    this.viewCalDate = day;
    this.viewDate = day.date;
    this.weeklyViewDate = day.date;
  }

  populateEvents(month: number, year: number) {
    if (this.scheduleKeys.includes(this.year + '.' + this.month)) {
      return;
    }
    this.scheduleService.getSchedule(month, year)
    .subscribe((response) => {
      this.mapToEvents(response);
      this.viewDate = new Date(year, month - 1, 1);
      this.viewDateChange();
      this.scheduleKeys.push(response.id);
    },
    (error) => {
      if (error.status === 404) {
        this.scheduleService.createSchedule(month, year)
        .subscribe(() => {this.populateEvents(month, year); }
      );
      } else {
        this.scheduleKeys.push(error.error);
      }
    });
  }

  eventActionHandler(engineer: EngineerModel, day: Date) {
    const model = new AllocationModel();
    model.calendarDate = day;
    model.engineer = engineer;
    this.dataService.allocateFrom.next(model);
  }

  getEventActions(engineer: EngineerModel, day: Date) {
    let actions = [];
    if (engineer && day > new Date()) {
      actions = [
        {
          label: 'Request a swap <i class="fa fa-spinner"></i>',
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventActionHandler(engineer, day);
          }
        }];
    }
    return actions;
  }

  mapToEvents(model: ScheduleModel) {
    if (!model.scheduledDayModels) {
      return false;
    }

    model.scheduledDayModels.forEach((dayModel) => {
      const thisDay = new Date(dayModel.calendarDay);
        if (dayModel.amAssignment) {
          const amEvent: CalendarEvent = {
              start: thisDay,
              end: thisDay,
              title: dayModel.amAssignment.name,
              meta: { 'key': model.id, 'model': dayModel, 'id': dayModel.amAssignment.id},
              actions: this.getEventActions(dayModel.amAssignment, thisDay)
          };
          amEvent.start.setHours(environment.am_start_hour);
          amEvent.end.setHours(environment.am_end_hour);
          this.events.push(amEvent);
        }

        if (dayModel.pmAssignment) {
          const pmEvent: CalendarEvent = {
              start: thisDay,
              end: thisDay,
              title: dayModel.pmAssignment.name,
              meta: { 'key': model.id, 'model': dayModel, 'id': dayModel.pmAssignment.id },
              actions: this.getEventActions(dayModel.pmAssignment, thisDay)
          };
          pmEvent.start.setHours(environment.pm_start_hour);
          pmEvent.end.setHours(environment.pm_end_hour);
          this.events.push(pmEvent);
        }
    });
  }
}

