import { Component, OnInit, TemplateRef, ViewChild, ContentChild, AfterViewInit, AfterContentChecked  } from '@angular/core';
import { EngineerModel, SupportDayModel, AllocationModel, SwapRequestModel } from '../../models/schedule-model';
import { ScheduleService } from 'src/services/schedule.service';
import { DataService } from 'src/services/data.service';
import { BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-swap',
  templateUrl: './swap.component.html',
  styleUrls: ['./swap.component.css']
})

export class SwapComponent implements OnInit {
  engineerTo: EngineerModel;
  dateTo: SupportDayModel;

  allocateFrom: AllocationModel;
  allocateTo: AllocationModel;
  swapAllocations: AllocationModel[] = null;
  serviceError: any;

  isShown() {
    return this.swapAllocations !== null && this.swapAllocations.length > 0;
  }

  swap(allocation: AllocationModel) {
    this.allocateTo = allocation;
    const request = new SwapRequestModel();
    request.dateFrom = this.allocateFrom.calendarDate;
    request.dateTo = this.allocateTo.calendarDate;
    request.engineerFromId = this.allocateFrom.engineer.id;
    request.engineerToId = this.allocateTo.engineer.id;
    request.sourceTimeSlot = this.allocateFrom.slot;
    request.destinationTimeSlot = this.allocateTo.slot;
    this.scheduleService.createSwap(request).subscribe(() => {
      const keyFrom = this.allocateFrom.calendarDate.getFullYear() + '.' + (this.allocateFrom.calendarDate.getMonth() + 1);
      const keyTo = this.allocateTo.calendarDate.getFullYear() + '.' + (this.allocateFrom.calendarDate.getMonth() + 1);
      const keys = [];
      keys.push(keyFrom);
      if (keyTo !== keyFrom) { keys.push(keyTo); }
      this.dataService.updateMonthScheduleKeys.next(keys);
      this.swapAllocations = null;
    });
  }

  constructor(private scheduleService: ScheduleService,  private dataService: DataService, private modalService: BsModalService) { }

  subscribeGetAllocations() {
    this.dataService.allocateFrom.subscribe((allocateFrom) => {
      this.allocateFrom = allocateFrom;
      this.scheduleService.getSwapAllocations(allocateFrom.engineer.id, allocateFrom.calendarDate)
        .subscribe((response) => {
          if (response) {
            this.swapAllocations = response.map((r) => {
              const allocation = new AllocationModel();
              allocation.calendarDate = new Date(r.calendarDate);
              allocation.engineer = r.engineer;
              allocation.slot = r.slot;
              return allocation;
              });
          } else {
            this.swapAllocations = null;
          }
        },
        (error) => { this.serviceError = error; });
  });
  }

  ngOnInit() {
    this.subscribeGetAllocations();
  }
}

